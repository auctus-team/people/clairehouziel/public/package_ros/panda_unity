#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH='/home/chouziel/panda_unity/devel/.private/catkin_tools_prebuild:/opt/ros/noetic:/opt/openrobots'
export LD_LIBRARY_PATH='/opt/ros/noetic/lib:/opt/ros/noetic/lib/x86_64-linux-gnu:/opt/openrobots/lib'
export PKG_CONFIG_PATH='/opt/ros/noetic/lib/pkgconfig:/opt/ros/noetic/lib/x86_64-linux-gnu/pkgconfig:/opt/openrobots/lib/pkgconfig'
export PWD='/home/chouziel/panda_unity/build/catkin_tools_prebuild'
export PYTHONPATH='/opt/ros/noetic/lib/python3/dist-packages:/opt/openrobots/lib/python3.10/site-packages'
export ROSLISP_PACKAGE_DIRECTORIES='/home/chouziel/panda_unity/devel/.private/catkin_tools_prebuild/share/common-lisp'
export ROS_PACKAGE_PATH="/home/chouziel/panda_unity/build/catkin_tools_prebuild:$ROS_PACKAGE_PATH"