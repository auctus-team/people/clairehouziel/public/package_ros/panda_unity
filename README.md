# Présentation 

## Nodes 

Ce package est formé de trois scripts qui fonctionnent avec le projet Unity. 

- le node [effector_position](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity/-/blob/master/src/effector_position.py) publie la pose de l'effecteur du robot par rapport à la base du robot dans le topic /effector_coordinates. Il n'a finalement pas été nécessaire au projet Unity, puisque ce calcul peut être fait directement par Unity. 

- le node [velocity_effector](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity/-/blob/master/src/velocity_effector.py) publie le Twist du vecteur vitesse de l'effecteur par rapport au repère de l'effecteur dans le topic /velocity_effector. Il fonctionne avec le projet [Hololens_vecteur_vitesse](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projets_unity/hololens_vecteur_vitesse).

- le node [cube_subscriber](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity/-/blob/master/src/cube_subscriber.py) reçoit les coordonnées du cube le plus proche de l'effecteur par rapport au repère ROS de la base du robot. Il fonctionne avec le projet [Hololens_objects](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projets_unity/hololens_objects), mais n'est pas nécessaire à son fonctionnement. 

## Launch 

Les launchs utilisés sont des launchs dérivés de panda_capacity. 
- [one_panda](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity/-/blob/master/launch/one_panda.launch) permet d'interagir avec un robot en simulation sur rviz
- [one_panda_real](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity/-/blob/master/launch/one_panda_real.launch) permet d'interagir avec le robot réel manuellement
- [one_panda_motion](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity/-/blob/master/launch/one_panda_motion.launch) permet le contrôle du mouvement du panda par Motion Planning.

Ils nécessitent donc les repositories :
- [panda_capacity](https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/ros_nodes/panda_capacity)
- [franka_ros](https://gitlab.inria.fr/auctus-team/components/robots/panda/franka_ros) 
- [panda_motion](https://gitlab.inria.fr/auctus-team/components/motion-planning/panda_motion) 

Votre launch peut suffire amplement, s'il y a un publisher dans joint-states et que l'URDF concerné est un panda_arm sans table. Il faut alors lui ajouter le connecteur à Unity  : 

- ajouter le package [ROS TCP Endpoint](https://github.com/Unity-Technologies/ROS-TCP-Endpoint) dans le workspace ROS : 


```
cd panda_capacity_ws/src
git clone https://github.com/Unity-Technologies/ROS-TCP-Endpoint.git
```



- dans le fichier launch de ce package (endpoint.launch), modifier la valeur de "tcp-ip" pour l'adresse IP de la machine utilisée (obtenue grâce à "\$ ifconfig" dans le terminal, section "inet") et la valeur de "tcp_port" à 10000 (valeur présente dans Unity > Robotics > ROS Settings > ROS Port)

- copier-coller l'intérieur de ce fichier launch dans le fichier launch utilisé


## Lien avec Unity

Une fois que le launch est lancé, vous pourrez voir l'hologramme lié à la configuration de votre robot. 
