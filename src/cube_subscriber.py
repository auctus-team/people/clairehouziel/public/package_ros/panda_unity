#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Pose

#initialisation des coordonnées
p = [0,0,0]
q = [0,0,0,0]

#on récupère les coordonnées du cube par rapport à la base du robot
def cube_callback (data) :
    global p,q
    if data.position != p or data.orientation != q : 
        p = data.position
        q = data.orientation
        rospy.loginfo(f'Cube : Position ({p}) Quaternion : ({q})')
    
    

if __name__ == '__main__' :
    try :
        rospy.init_node('cube_subscriber', anonymous=True)
        sub = rospy.Subscriber('/cube_position', Pose, cube_callback)

        rospy.spin()


    except rospy.ROSInterruptException : pass